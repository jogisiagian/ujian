package TestLogic;

import java.util.Scanner;

public class VokalKonsonan {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);

		System.out.print("input n: ");
		String kt = kb.nextLine().replace(" ", "").toLowerCase();

		String[] aAbjad = kt.split("");
		char[] aChar = new char[aAbjad.length];

		// fill
		for (int i = 0; i < aChar.length; i++) {
			aChar[i] = aAbjad[i].charAt(0);
		}

		// print before sort
//		for (char s : aChar) {
//			System.out.print(s + " ");
//		}
		// sort
		for (int x = 0; x < aChar.length; x++) {
			for (int i = 0; i < aAbjad.length - 1; i++) {
				char wadah = aChar[i];
				if (aChar[i] > aChar[i + 1]) {
					aChar[i] = aChar[i + 1];
					aChar[i + 1] = wadah;
				}
			}
		}

		// print after sort
//		System.out.println();
//		for (char c : aChar) {
//			System.out.print(c + " ");
//		}

		String wadah = "";

		for (int i = 0; i < aChar.length; i++) {
			// System.out.print(aChar[i]);
			wadah = wadah + aChar[i];

		}
		// System.out.println(wadah);
//		System.out.println();
		String[] arr = wadah.split("");
		String a = "";
		String b = "";
		String c = "";
		for (int x = 0; x < arr.length; x++) {
			if (arr[x].equalsIgnoreCase("a") || arr[x].equalsIgnoreCase("i") || arr[x].equalsIgnoreCase("u")
					|| arr[x].equalsIgnoreCase("e") || arr[x].equalsIgnoreCase("o")) {
				a = a + arr[x];

			} else if ((arr[x].equalsIgnoreCase("b") || arr[x].equalsIgnoreCase("c") || arr[x].equalsIgnoreCase("d")
					|| arr[x].equalsIgnoreCase("f") || arr[x].equalsIgnoreCase("g")|| arr[x].equalsIgnoreCase("h")|| arr[x].equalsIgnoreCase("g"))|| arr[x].equalsIgnoreCase("h")
					|| arr[x].equalsIgnoreCase("j")|| arr[x].equalsIgnoreCase("k")|| arr[x].equalsIgnoreCase("l")|| arr[x].equalsIgnoreCase("m")|| arr[x].equalsIgnoreCase("n")
					|| arr[x].equalsIgnoreCase("p")|| arr[x].equalsIgnoreCase("q")|| arr[x].equalsIgnoreCase("r")|| arr[x].equalsIgnoreCase("s")|| arr[x].equalsIgnoreCase("t")
					|| arr[x].equalsIgnoreCase("v")|| arr[x].equalsIgnoreCase("w")|| arr[x].equalsIgnoreCase("x")|| arr[x].equalsIgnoreCase("y")|| arr[x].equalsIgnoreCase("z")){
				b = b + arr[x];
			} else {
				c = c + arr[x];
			}
		}

		System.out.print("huruf vokal: ");
		System.out.print(a);
		System.out.println();
		System.out.print("huruf konsonan: ");
		System.out.print(b);
		System.out.println();
		System.out.print("others: ");
		System.out.print(c);
		kb.close();

	}

}
