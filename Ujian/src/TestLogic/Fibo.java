package TestLogic;

import java.util.Scanner;

public class Fibo {
//	Berapa banyak angka fibonacci selain nol dibawah x yang merupakan angka genap
//
//	Input x = ?
//	Output: Sebanyak ...

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		System.out.println("input x: ");
		int n = s.nextInt();
		
		
		int fb = 1;
		int x = 1;
		int y = 0;
		int counter = 0;

		for (int i = 0; i < n; i++) {
			x = y;
			y = fb;
			System.out.print(fb + " ");
			if (fb%2==0) {
				counter++;
//				
				
			}
			fb = x + y;
			if(fb>=n)break;
		}
		System.out.println("Sebanyak= " +counter);
		s.close();

	}

}
