package TestLogic;

import java.util.Scanner;

public class Alfabet {
//	Huruf alfabet dalam huruf kecil di bawah ini mengandung bobot yang sudah ditentukan sebagai berikut:
//		a = 1; b = 2; c = 3; d = 4; .... Z = 26.
//		Tentukan apakah dalam sebuah input string sudah memiliki bobot yang sesuai.
//
//		Constraint :
//		-    0 <= n <= 100
//		-    string hanya mengandung huruf kecil
//
//		Input
//		string : mengandung kata/kalimat
//		array n : mengandung array angka yang harus dicocokkan terhadap string
//
//		Example
//		string : abcdzzz
//		array : [1, 2, 2, 4, 4, 26, 26]
//
//		Output : true, true, false, true, false, true, true

	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		
		System.out.print("input alphabet: ");
		String input = s.nextLine();
		System.out.print("input angka: ");
		int iAngka = s.nextInt();
		
		String alfabet = "abcdefghijklmnopqrstuvwxyz";
		String[]aAlfabet = alfabet.split("");
		
		char[]abc = new char[aAlfabet.length];
		for (int i = 0; i < aAlfabet.length; i++) {
			abc[i] = aAlfabet[i].charAt(0);
		}
		
		
		char start = 'a';
		for (int i = 0; i <aAlfabet.length; i++) {
			abc[i] = start;
			start++;
		}
		
		for (char c : abc) {
			System.out.print(c + " ");
		}
		
		
		
//		String[]aAngka =iAngka.split(",");
//		
//		
//		
//		int []aAngka2 = new int [aAngka.length];
//		
//		for (int i = 0; i < aAngka2.length; i++) {
//			aAngka2[i] = Integer.parseInt(aAngka[i]);
//		}
//		
//		for (String string : aAngka) {
//			System.out.print(string + " ");
//		}
//		
//		if ('a'== 1) System.out.println("test");
//		
//		
//		String[]aAZ = new String[25];
		s.close();
	}

}
