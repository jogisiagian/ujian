package TestLogic;

import java.util.Scanner;
//Seringkali kita melihat data yang dienkripsi terhadap informasi yang bersifat rahasia. Beragam metode enkripsi telah dikembangkan hingga saat ini. Kali ini kalian akan ditugaskan untuk membuat metode enkripsi sendiri agar informasi yang diinput menjadi aman, dengan menggunakan original alfabet yang dirotasi pada enkripsi tersebut.
//
//Constraint :
//-    Original Alfabet : abcdefghijklmnopqrstuvwxyz
//-    Semua enkripsi hanya menggunakan huruf kecil
//-    Spasi/karakter spesial tidak dianggap
//
//Input :
//string : mengandung data yang belum dienkripsi
//n : jumlah huruf yang digunakan untuk merotasi original alfabet (0 <= n <= 100)
//
//Example
//string : ba ca
//n : 3
//
//Output
//Original Alfabet : abcdefghijklmnopqrstuvwxyz
//Alfabet yang dirotasi : defghijklmnopqrstuvwxyzabc
//Hasil enkripsi : edfd

public class Enkripsi {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		System.out.print("String: ");
		String input = s.nextLine().replace(" ", "").toLowerCase();
		System.out.print("n: ");
		int n = s.nextInt();
		
		String alfabet = "abcdefghijklmnopqrstuvwxyz";
		String alfabet2 = "zyxwvutsrqponmlkjihgfedcba";
		String[] aAlfabet = alfabet.split("");
		String[] aAlfabetbalik = alfabet2.split("");
		
		char[]aAlfabet2 = new char[aAlfabet.length];
		char[]aOrigin = new char[aAlfabet.length];
		char[]aBalik = new char[aAlfabet.length];

		// fill
		for (int i = 0; i < aAlfabet.length; i++) {
			aAlfabet2[i] = aAlfabet[i].charAt(0);
			aOrigin[i] = aAlfabet[i].charAt(0);
			aBalik[i] = aAlfabetbalik[i].charAt(0);
			
		}
		
		
		
		System.out.print("Original: ");
		for (char a : aAlfabet2) {
			System.out.print(a);
		}
		
//		System.out.println(aAlfabet[0].charAt(0));
		int counter = 0;
		char temp= 0;
		char temp2=0;
		
		for (int i = 0; i < n; i++) {
			
			temp = aAlfabet2[0];
			for (int j = 0; j < aAlfabet2.length-1; j++) {				
				
				aAlfabet2[j]= aAlfabet2[j+1];
				
			}
			aAlfabet2[aAlfabet2.length-1]=temp;
	
		}
		System.out.println();
		System.out.print("Enkripsi: ");
//		for (char a : aAlfabet2) {
//			System.out.print(a);
//		}
		for (char a : aBalik) {
			System.out.print(a);
		}
		
		String[]aInput = input.split("");
		char[]aInput2 = new char [aInput.length];
		
		// fill
		for (int i = 0; i < aInput2.length; i++) {
			aInput2[i] = aInput[i].charAt(0);
		}
		System.out.println();
		
//		for (char a : aInput2) {
//			System.out.print(a);
//		}
		String wadah="";
		System.out.println();
		System.out.println("hasil: ");
		int c = 0;
		for (int i = 0; i < aInput2.length; i++) {
			for (int j = 0; j < aAlfabet.length; j++) {
				if(aInput2[i]==aBalik[j]) {
					wadah+=aAlfabet2[j];
					 counter++;
					
				}
				
			}
			
		}
		
		System.out.print(wadah);
		System.out.println();
		System.out.println(c);
		
		
		
		
		
		s.close();
	}

}
