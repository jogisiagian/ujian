package TestLogic;

import java.util.Scanner;
//Anda akan menggunting-gunting tali sepanjang z meter menjadi beberapa buah tali sepanjang x meter. 
//Berapa kali sedikitnya anda akan menggunting tali tersebut ?
//Contoh: z = 4, x = 1
//Cukup menggunting 2x (pertama, tali 4m dibagi 2 sama rata, akan didapatkan masing-masing 2m. 
//Kemudian kedua tali 2m itu dipotong bersama sama rata, akan dihasilkan 4 tali masing-masing panjang 1m)

public class Tali {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		System.out.print("Panjang tali: ");
		int tali = s.nextInt();
		System.out.print("bagi menjadi tali sepanjang: ");
		int pTali = s.nextInt();
		int counter=0;
		
		int sTali=0;
		
//		if (tali>0) {
//			tali= tali/2;
//		}
		if (tali%2==0) {
			while (tali>pTali) {
				tali= tali/2;
				counter++;
			}
		} else {
			tali-=1;
			counter++;
			while (tali>pTali) {
				tali= tali/2;
				counter++;
			}
		}
			
		if (tali==pTali) {
//			System.out.println(tali);
			System.out.println("cukup menggunting " + counter + " x");
		}
		

	}

}
