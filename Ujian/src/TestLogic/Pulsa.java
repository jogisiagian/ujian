package TestLogic;

import java.util.Scanner;

public class Pulsa {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);

		System.out.print("Beli pulsa Rp. ");
		int pulsa = s.nextInt();
		int poin1 = 0;
		int poin2 = 0;
		int poin3 = 0;
		int poin4 = 0;
		int total = 0;

		if (pulsa > 50000) {
			pulsa = pulsa - 50000;
			poin4 = (pulsa / 1000) * 3;
			poin3 = (30000 / 1000) * 2;
			poin2 = (20000 / 1000);
			total = poin4 + poin3 + poin2 + poin1;
			System.out.println("poin: " + poin1 + " + " + poin2 + " + " + poin3 + " + " + poin4 + " = " + total + " point");

		} else if (pulsa > 30000) {
			pulsa = pulsa - 30000;
			poin3 = (pulsa / 1000) * 2;
			poin2 = (20000 / 1000);
			total = poin3 + poin2 + poin1;
			System.out.println("poin: " + poin1 + " + " + poin2 + " + " + poin3 + " = " + total + " point");
		} else if (pulsa > 10000) {
			pulsa = pulsa - 10000;
			poin2 = (pulsa / 1000);
			total = poin2 + poin1;
			System.out.println("poin: " + poin1 + " + " + poin2 + " = " + total + " point");
		} else {
			poin1 = 0;
			total = poin1;
			System.out.println("poin: " + poin1 + " = " + total + " point");
		}

		s.close();

	}

}
