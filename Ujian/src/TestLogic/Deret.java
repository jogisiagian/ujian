package TestLogic;

import java.util.Iterator;
import java.util.Scanner;
//Buatlah deret angka yang terbentuk dari penjumlahan deret bilangan kelipatan 3 dikurang 1 dan deret bilangan kelipatan 4 diibagi(/) 2. Angka pada index ganjil dari kedua deret bilangan tersebut saling dijumlahkan. Dan angka pada index genap dari kedua deret bilangan tersebut juga saling dijumlahkan. Index dimulai dari angka 0.
//Input : Panjang array/panjang deret
//Contoh : Dibawah ini hanya sekedar contoh yang menggunakan deret genap dan ganjil
//
//Input panjang deret : 5
//Deret genap : 0 2 4 6 8
//Deret ganjil : 1 3 5 7 9
//0 + 1 ; 2 + 3 ; 4 + 5 ; 6 + 7 ; 8 + 9
//
//Output : 1, 5, 9, 13, 17

public class Deret {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		System.out.print("Panjang deret");
		int p = s.nextInt();
		
		int []kelipatan1 = new int[p];
		int []kelipatan2 = new int[p];
		
		int kel1= 0;
		for (int i = 0; i < kelipatan1.length; i++) {
			kelipatan1[i] = kel1-1;
			kel1+=3;
			
		}
		
		for (int i : kelipatan1) {
//			if (i!=0)
//			System.out.print(i-1 + " ");
//			else 
				System.out.print(i + " ");
		}
		
		int kel2= 0;
		for (int i = 0; i < kelipatan2.length; i++) {
			kelipatan2[i] = kel2/2;
			kel2+=4;
			
		}
		System.out.println();
		for (int i : kelipatan2) {
			System.out.print(i + " ");
		}
		System.out.println();
//		System.out.print("sum genap :");
		int jGenap=0;
//		for (int i = 0; i < p; i++) {
//			if (i%2 == 0) {
//				if (i==p-1) {
//					System.out.print(kelipatan1[i]+kelipatan2[i]);
//					jGenap=jGenap+(kelipatan1[i]+kelipatan2[i]);
//				}else
//				System.out.print(kelipatan1[i]+kelipatan2[i]+" , ");
//				jGenap=jGenap+(kelipatan1[i]+kelipatan2[i]);
//			}
//			//System.out.print(ganjil+" ");
//		}
//		System.out.print(" = "+jGenap);
		
		for (int i = 0; i < p; i++) {
			System.out.print(kelipatan1[i] + kelipatan2[i] + ", ");
			
		}
		
		
		s.close();

	}

}
