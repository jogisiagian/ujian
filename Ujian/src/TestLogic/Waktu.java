package TestLogic;

import java.util.Scanner;
//Dengan hanya menggunakan logic, ubah format jam dari 24H ke 12H dan juga sebaliknya
//
//contoh:
//input: 12:35 AM
//output: 00:35
//
//input: 19:30
//output: 07:30 PM
//
//input: 09:05
//output: 09:05 AM
//
//input: 11:30 PM
//output: 23:30
//
//NB: perbedaan format 12H dan 24H ada pada jam 00 tengah malam (jam 00 adalah jam 12 AM, lihat contoh 1), selebihnya tinggal menambahkan PM antara jam 12:00 - 23:59 dan AM antara jam 00:00 - 11:59


public class Waktu {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		System.out.print("Input waktu: ");
		String amPm = s.nextLine().replace(" ", "").toUpperCase();
//		String amPm = "12:43AM";
		int hh = Integer.parseInt(amPm.substring(0,2));
		int mm = Integer.parseInt(amPm.substring(3,5));
		String[] amPm2 = amPm.split("");
//		System.out.println(mm);
		
		if (amPm.length() == 5) {
			if (hh>12 && mm==0) {
				hh=hh-12;
				System.out.print(hh);
				System.out.print(amPm.substring(2,5) + " PM");
			} else if (hh==0){
				hh=hh+12;
				System.out.print(hh);
				System.out.print(amPm.substring(2,5) + " AM");
				
			} else if (hh<12) {
				System.out.print(amPm.substring(0,5) + " AM");
			} 
			else if (hh>12) {
				hh=hh-12;
				System.out.print(hh);
				System.out.print(amPm.substring(2,5) + " PM");
			} 
			else {
				System.out.print(amPm.substring(0,5) + " PM");
			}
			
			
		} else {
		if (amPm.charAt(5) == 'A') {
			if (hh == 12) {
				hh=00;
				//cetak hh
				System.out.print("00");
				//cetak mm,ss
				System.out.print(amPm.substring(2,5));
			} else {
				System.out.print(amPm.substring(0,5));
				
			}
		} else {
			if (hh== 12) {
				System.out.print(amPm.substring(0,5));
			} else {
				hh= hh+12;
				//cetak hh
				System.out.print(hh);
				//cetak mm,ss
				System.out.print(amPm.substring(2,5));
			}
		}
		}
		
		s.close();
	}

}
